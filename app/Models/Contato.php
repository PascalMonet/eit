<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{

    protected $fillable = [
        'laboratorio_id', 'email', 'atendimento', 'local', 'telefone'
    ];

    public function laboratorio()
    {
        return $this->belongsTo('App\Models\Laboratorio');
    }
}
