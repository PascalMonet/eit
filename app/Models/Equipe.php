<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $fillable = [
        'laboratorio_id', 'nome'
    ];

    public function laboratorio()
    {
        return $this->belongsTo('App\Models\Laboratorio');
    }
}
