<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'area_atuacao', 'descricao' , 'telefone' , 'email', 'atendimento' , 'local'
    ];

    public function contatos()
    {
        return $this->hasMany('App\Models\Contato');
    }
    public function projetosLab()
    {
        return $this->hasMany('App\Models\ProjetoLab');
    }
    public function responsaveisLab(){
        return $this->hasMany('App\Models\ResponsavelLab');
    }

    public function equipe(){
        return $this->hasMany('App\Models\Equipe');

    }
}
