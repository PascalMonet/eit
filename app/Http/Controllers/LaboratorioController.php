<?php

namespace App\Http\Controllers;

use App\Models\Laboratorio;
use Illuminate\Http\Request;

class LaboratorioController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->only(['edit','update','store']);

    }

    public function index()
    {
        $bd = Laboratorio::all();
        return view('laboratorio.pesquisarlab')->with('bd', $bd);
    }
    public function show(Laboratorio $laboratorio){
        return view('laboratorio.show')->with('lab',$laboratorio);
    }
    public function principal(){
        return view('laboratorio.index')->name('principal');
    }
    public function edit($id)
    {
        $laboratorio = Laboratorio::findOrFail($id);
        return view('laboratorio.edit')->with('lab', $laboratorio);
    }


    public function update(Request $request, Laboratorio $laboratorio)
    {
        $laboratorio->update($request->all());
        return $laboratorio;

    }

    public function create(){
        return view('laboratorio.create');
    }
    public function store(Request $request)
    {
        // Regras de validação.
        $rules = [
            'titulo' => 'required|min:3|max:50',
            'area_atuacao' => 'required',
            'descricao' => 'required'
        ];
        $this->validate($request, $rules);
        if (Laboratorio::create($request->all())) {
            return redirect()->route('home');
        } else {
            return redirect()->route('home');
        }
    }
}
