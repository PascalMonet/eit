<?php

namespace App\Http\Controllers;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller

{
    public function __construct()
    {
        $this->middleware('auth')->only(['edit', 'update', 'store']);

    }

    public function index()
    {
        $post = Post::paginate(4);
        return view('welcome')->with('bd', $post);
    }

    public function create()
    {
        return view('post.criarpost');
    }

    public function show(Post $post){
        return view('post.show')->with('post',$post);
    }

    public function store(Request $request)
    {/*
        $rules = [
            'titulo' => 'required|min:3|max:50',
            'descricao' => 'required',
            'post' => 'required'
        ];
*/
        Post::create($request->all());
        return redirect()->route('home');

    }
}
