<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EditarLaboratorioController extends Controller
{
    public function select($id)
    {
        $laboratorio = Laboratorio::findOrFail($id);
        return view('laboratorio.EditarLaboratorio') ->with('lab', $laboratorio);
    }
}
