<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContatoLab extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->string('telefone');
            $table->string('email');
            $table->date('atendimento');
            $table->integer('laboratorio_id')->unsigned();
            $table->foreign('laboratorio_id')
                ->references('id')
                ->on('laboratorios')
                ->onDelete('cascade');
            $table->string('local');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
