<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetoLabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projeto_labs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome');

            $table->integer('laboratorio_id')->unsigned();
            $table->foreign('laboratorio_id')
                ->references('id')
                ->on('laboratorios')
                ->onDelete('cascade');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projeto_labs');
    }
}
