<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    @extends('layouts.link')
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>

<!-- Menu, não mudar entre as novas abas -->
@include('Layouts.Nav')
<div class="container-fluid tela">
    <div style="text-align: center;">
        <h1 class="titulo">Cadastrar laboratórios</h1>
        <div class="row">
            <div class="col col-md-3"></div>
            <div class="col col-md-6">
                <form method="post" class="form" action="{{route('laboratorios.store')}}">
                    {{csrf_field()}}
                    <div class="form-group">

                        <div class="row ">
                            <div class="col col-md-3 CadastroLab">
                                <label>Nome do laboratório</label>
                            </div>
                            <div class="col col-md-9 ">
                                <input type="text" class="form-control" name="titulo" placeholder="Digite o nome">
                            </div>
                        </div>
                        <!-- area de atuacao -->
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Área de atuação: </label>

                            </div>
                            <div class="col col-md-9">
                                <input required type="text" class="form-control" name="area_atuacao"
                                       placeholder="Digite a área">
                            </div>
                        </div>
                        <!-- especialidade -->
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Especialidade: </label>

                            </div>
                            <div class="col col-md-9">
                                <input required type="text" class="form-control" name="especialidade"
                                       placeholder="Digite a especialidade">
                            </div>
                        </div>
                        <!-- Descrição -->
                        <div class="row" style="margin-bottom:10%;">
                            <div class="col col-md-3">
                                <label>Descrição: </label>
                            </div>
                            <div class="col col-md-9">
                                    <textarea class="form-control" name="descricao" required rows="5" cols="71"
                                              placeholder="Digite a descrição"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Imagem</label>

                            </div>
                            <div class="col col-md-5">
                                <input type="file" class="form-control"
                                       placeholder="Digite a área">
                            </div>
                        </div>

                        <!-- Contatos -->
                        <h2 class="titulocontato">Contato</h2>

                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Telefone: </label>
                            </div>
                            <div class="col col-md-9">
                                <input class="form-control" name="telefone" required
                                       placeholder="Digite o telefone">
                            </div>
                        </div>
                        <!-- E-mail -->
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>E-mail: </label>
                            </div>
                            <div class="col col-md-9">
                                <input class="form-control" type="email" name="email" required
                                       placeholder="Digite o E-mail">
                            </div>
                        </div>
                        <!-- Atendimento -->
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Horário de atendimento: </label>
                            </div>
                            <div class="col col-md-9">
                                <input class="form-control" type="text" name="atendimento" required
                                       placeholder="Digite o horário">
                            </div>
                        </div>
                        <!-- local -->
                        <div class="row">
                            <div class="col col-md-3 CadastroLab">
                                <label>Local: </label>
                            </div>
                            <div class="col col-md-9">
                                <input class="form-control" type="text" name="local" required
                                       placeholder="Digite o Local">
                            </div>
                        </div>
                        <!--  -->

                    </div>
                    <!-- Equipe -->
                    <div class="card" id="equipe">
                        <h1 class="titulocontato card-header">Equipe</h1>
                        <div id="equipe">
                            <div class="row card-body">
                                <div class="col col-md-5">
                                    <div class="row">
                                        <div class="col col-md-3 CadastroLab">
                                            <label>Nome </label>
                                        </div>
                                        <div class="col col-md-9">
                                            <input class="form-control" v-model="novapessoa" type="text" name="local"
                                                   required
                                                   placeholder="Digite o nome">
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-md-5">
                                    <div class="row">
                                        <div class="col col-md-3 CadastroLab">
                                            <label>Função </label>
                                        </div>
                                        <div class="col col-md-9">
                                            <select name="funcao">
                                                <option value="Aluno"> Aluno</option>
                                                <option value="responsavel">Responsavel</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-md-2">
                                    <button class="btn btn-success" v-on:click="adicionarPessoa">Adicionar</button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <table>
                                <thead>
                                <tr>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Funçao</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <th></th>
                                    <th></th>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="posicaobotao row">
                        <div class="col col-md-6">
                            <button class="btn btn-danger btn-lg">Cancelar</button>
                        </div>
                        <div class="col col-md-6">
                            <button type="submit" class="btn btn-info btn-lg">Cadastrar</button>
                        </div>
                    </div>
                    <!-- Projetos Ver como adicionar vários projetos.-->
                </form>
            </div>
            <div class="col col-md-3"></div>
        </div>
        @include('Layouts.Footer')
    </div>

</div>
</body>
</html>