
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Importar arquivos AQUI -->
    <link href="{{url('css/style.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{url('css/bootstrap.css')}}"/>
    <link rel="stylesheet" href="css/dataTables.bootstrap4.css"/>

</head>

<body>

<!-- Menu, não mudar entre as novas abas -->
<div class="container-fluid tela">

    @extends('layouts.app')
    @section('content')
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Muito bem!</h4>
            <p>
                Laboratório cadastrado com sucesso
            </p>
            <hr>
            <p class="mb-0">Clique no botão abaixo para cadastrar mais um laboratório</p>
            <a href="{{route('cadastrarlab')}}"class="btn btn-info" style="margin-top:20px;">Adicionar mais</a>
        </div>
</div>
@endsection
</body>
</html>

