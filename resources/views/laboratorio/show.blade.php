<!DOCTYPE html>
@include('layouts.link')
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<!-- Menu, não mudar entre as novas abas -->

<div class="container-fluid">

@include('Layouts.Nav')

<!-- TÍTULO -->
    @guest
    @else
        <i class="far fa-edit"></i>
        <a href="/eit/public/laboratorios/{{$lab->id}}/edit">Editar</a>
    @endguest
    <div style="text-align: center;">
        <h1 class="titulolab">
            {{$lab->titulo}}
        </h1>
    </div>
    <!--  Fim  -->
    <!-- Slides -->
    <div class="row slidecomtexto">
        <div class="col col-md-7">
            <div id="carouselExampleControls" class="carousel slide slidepersonalizado" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="../images/1.jpg" height="500px" width="350px"
                             alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="../images/2.png" height="500px" width="350px"
                             alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="../images/3.jpg" height="500px" width="350px"
                             alt="Third slide">
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col col-md-5 descricao">
            <h2>Área de atuação: {{$lab->area_atuacao}}</h2>
            <br/>
            <p> {{$lab->descricao}} </p>
        </div>
    </div>
    <hr class="linhaabaixo"/>
    <!-- Fim do Slide e descrição -->
    <!-- Nome dos responsáveis -->


    <div class="row">
        <div class="col col-md-1"></div>
        <div class="col col-md-5 ">
            <h2 class="responsaveis" style="text-align: center;">
                <i class="fas fa-user"></i>
                Responsáveis
            </h2>
            <table class="table table-hover tabelaresponsaveistd" id="tabelaresponsavel">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Curriculo</th>
                    <th>Função</th>
                </tr>
                </thead>
                <tr>
                    <td>Pessoa1</td>
                    <td><a href="www.google.com.br" TARGET="_blank">google</a></td>
                    <td>Diretor</td>
                </tr>
                <tr>
                    <td>Pessoa2</td>
                    <td><a HREF="www.youtube.com.br" TARGET="_blank">youtube</a></td>
                    <td>Coordenador</td>
                </tr>
            </table>
        </div>
        <div class="col col-md-5">
            <h2 style="text-align:center;" class="responsaveis"><i class="fas fa-users"></i>
                Equipe
            </h2>
            <table class="table table-hover tabelaresponsaveistd" id="tabelaEquipe">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Curriculo</th>
                    <th>Função</th>
                </tr>
                </thead>
                <tr>
                    <td>Pessoa1</td>
                    <td><a href="www.google.com.br" TARGET="_blank">google</a></td>
                    <td>Diretor</td>
                </tr>
                <tr>
                    <td>Pessoa2</td>
                    <td><a HREF="www.youtube.com.br" TARGET="_blank">youtube</a></td>
                    <td>Coordenador</td>
                </tr>
            </table>
        </div>
        <div class="col col-md-1"></div>
    </div>
    <hr class="linhaabaixo"/>
    <!-- Fim do nome dos responsáveis e Equipe -->
    <div class="row">
        <div class="col col-md-2"></div>
        <div class="col col-md-8" style="margin-bottom:50px;">
            <h2 style="text-align: center; margin-top:50px;">
                <i class="fas fa-file-alt"></i>
                Projetos
            </h2>
            <table class="table table-hover tabelaresponsaveistd" id="tabelaProjetos">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Link para informações</th>
                    <th>Responsável</th>
                </tr>
                </thead>
                <tr>
                    <td>Extração de Dados</td>
                    <td><a href="www.google.com.br" TARGET="_blank">artigo</a></td>
                    <td>Ariel dos Santos</td>
                </tr>
                <tr>
                    <td>WEB</td>
                    <td><a HREF="www.youtube.com.br" TARGET="_blank">youtube</a></td>
                    <td>Pascal Monet</td>
                </tr>
            </table>
        </div>
        <div class="col col-md-2"></div>
    </div>
    <!-- Fim dos Projetos -->
    <hr class="linhaabaixo"/>
    <!-- Contatos -->
    <div class="row">
        <div class="col col-md-1"></div>
        <div class="col col-md-10" style="text-align: center; margin-top:20px;">
            <h2 class="contato">
                <a><i class="fas fa-phone-square"></i>
                    Contatos</a>
            </h2>
            <p>Telefone : {{$lab->telefone}}</p>
            <p>Email: {{$lab->email}}</p>
            <p>Horário de atendimento: {{$lab->atendimento}}</p>
            <p>Local do laboratório: {{$lab->local}}</p>
        </div>
        <div class="col col-md-1"></div>
    </div>
</div>
<!--Fim da aba Contatos -->
@include ('Layouts.Footer')

</body>
</html>
