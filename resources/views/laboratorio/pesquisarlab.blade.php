<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('layouts.link')
    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
@include('Layouts.Nav')
<!-- Menu, não mudar entre as novas abas -->


<!-- Conteudo do menu    inner cover   inicial-->
<div class="container-fluid">
    <div class="tela">
        <main role="main" class="body">
            <table id="idpesquisalab" class="table table-striped tabeladatatable">
                <thead class="thead-dark">
                <tr>
                    <div style="text-align:center;">
                        <th style="text-align:center;">Título</th>
                        <th>Área de atuação</th>
                        <th>Descrição</th>
                        <th>Local</th>
                        <th>#</th>
                    </div>
                </tr>
                </thead>
                <tbody>
                @foreach($bd as $bds)
                    <tr>
                        <td scope="row">
                            {{$bds->titulo}}
                        </td>
                        <td scope="row">
                            {{$bds->area_atuacao}}
                        </td>
                        <td scope="row">
                            {{$bds->descricao}}
                        </td>
                        <td scope="row">
                            {{$bds->local}}

                        </td>
                        <td>
                            <a class="btn btn-info" href="/eit/public/laboratorios/{{$bds->id}}">Acessar</a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </main>
    </div>
    @include('Layouts.Footer')
</div>
</body>
</html>
