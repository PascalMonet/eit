@extends('layouts.app')
@extends('layouts.link')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-2"></div>
            <div class="col col-md-8">

                <h1 class="text-center titulocadastrar"><i class="fas fa-database"></i>

                    EDITAR</h1>

                <hr/>
                <form method="post" class="form" action="{{route('laboratorios.update',$lab->id)}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="patch" />
                    <div class="form-group">
                        <label>Nome do laboratório</label>
                    </div>
                    <div class="form-gruop">
                        <input type="text" class="form-group" value="{{$lab->id}}">
                    </div>
                    <input type="text" value="{{$lab->titulo}}" class="form-control" name="titulo">
                    <div class="col col-md-3 CadastroLab">
                        <label>Área de atuação: </label>

                    </div>
                    <input required type="text" class="form-control" name="area_atuacao"
                           value="{{$lab->area_atuacao}}">
                    <!-- Descrição -->
                    <label>Descrição: </label>
                    <textarea class="form-control" name="descricao" required rows="5"
                              cols="71">{{$lab->descricao}}</textarea>
                    <label>Imagem</label>

                    <input type="file" class="form-control"
                           placeholder="Digite a área">

                    <!-- Contatos -->
                    <h2 class="titulocontato">Contato</h2>

                    <label>Telefone: </label>
                    <input class="form-control" name="telefone" required
                           value="{{$lab->telefone}}">
                    <!-- E-mail -->
                    <label>E-mail: </label>
                    <input class="form-control" type="email" name="email" required
                           value="{{$lab->email}}">
                    <!-- Atendimento -->
                    <label>Horário de atendimento: </label>
                    <input class="form-control" type="text" name="atendimento" required
                           value="{{$lab->atendimento}}">
                    <!-- local -->
                    <label>Local: </label>
                    <input class="form-control" type="text" name="local" required
                           value="{{$lab->local}}">
                    <!--  -->

                    <div class="posicaobotao">
                        <div class="row">
                            <div class="col col-md-6" align="center">
                                <button class="btn btn-danger btn-lg">Cancelar</button>
                            </div>
                            <div class="col col-md-6" align="center">
                                <button type="submit" class="btn btn-info ml-auto btn-lg">Salvar</button>
                            </div>
                        </div>
                    </div>
                    <!-- Projetos Ver como adicionar vários projetos.-->
                </form>


                <!-- Remover -->
            </div>
            <div class="col col-md-2"></div>
        </div>
    </div>
@endsection

