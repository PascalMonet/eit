<div style="max-height:600px; max-width:620px; width: auto; height: auto;" id="carouselExampleIndicators" class="carousel slide slideprincipal" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div  class="carousel-inner">
        <div class="carousel-item active zoom">
            <img  class="w-100" src="images/Menu1.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block fundodescricaoslide">
                <h5>Patente 1</h5>
                <p>Descricao da patente</p>
            </div>
        </div>
        <div class="carousel-item zoom">
            <img   class="w-100" src="images/2.png" alt="Second slide">
            <div class="carousel-caption d-none d-md-block ">
                <div class="fundodescricaoslide"></div>
                <h5>Patente 2</h5>
                <p>Descricao da patente</p>
            </div>
        </div>
        <div class="carousel-item zoom">
            <img height="50%" width="50%" class="w-100" src="images/3.jpg"alt="Third slide">
            <div class="carousel-caption d-none d-md-block fundodescricaoslide">
                <h5>Patente 3</h5>
                <p>Descricao da patente</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

