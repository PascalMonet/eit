<!DOCTYPE html>
@include('layouts.link')
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('Layouts.Nav')
</head>
<body>
<div class="container-fluid">
    <div class="tela">
        <main role="main" class="body">
            <div style="text-align: center;">
                <h1 class="titulo">
                    Escritório de Inovação Tecnológia
                </h1>
            </div>
            <div class="row">
                <div class="col col-md-12 ">
                    <div style="text-align: center; margin-bottom:5%; margin-top:10%;">
                        <div class="row">
                            <div class="col col-md-6">
                                <p class="descricaotitulo"> O Escritório de Inovação Tecnológica (EIT) é o
                                    Núcleo de
                                    Inovação
                                    Tecnológica
                                    da Universidade Federal de Mato Grosso, criado pela Resolução CD nº 18/2007
                                    e
                                    atualizadoA\
                                    por
                                    meio da Resolução CD nº 17/2016 em consonância com o Novo Marco Legal da
                                    Inovação
                                    conforme
                                    disposto na Lei nº 13.243/2016... ​</p>
                            </div>
                            <div class="col col-md-6">
                                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img class="d-block img-fluid" height="500px" width="450px"
                                                 src="images/Menu1.jpg" alt="First slide">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" height="500px" width="450px"
                                                 src="images/Menu2.jpeg" alt="Second slide">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block img-fluid" height="500px" width="450px"
                                                 src="images/Menu3.jpg" alt="Third slide">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls"
                                       role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls"
                                       role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <a class="btn btn-outline-primary saibamais" data-toggle="collapse"
                           href="#collapseExample"
                           role="button" aria-expanded="false" aria-controls="collapseExample">
                            Saiba mais...
                        </a>
                        </p>
                        <div class="collapse" id="collapseExample">
                            <p class="descricaotitulo"> Idealizado pelo Escritório de Inovação Tecnológica da
                                UFMT, a plataforma “Vitrine Tecnológica” foi constituída visando mapear e
                                apresentar à comunidade os principais produtos e serviços com valor agregado a
                                partir do Ensino, Pesquisa e Inovação do ambiente produtivo da Universidade.

                                Objetiva-se, com a plataforma, apresentar as competências desenvolvidas na
                                instituição e que são oferecidas por seus laboratórios, a fim de promover a
                                interação e a formação de parcerias entre a Universidade Federal de Mato Grosso
                                e outras instituições públicas ou privadas, engajadas na geração de
                                conhecimento, inovações e valores para a sociedade.

                                Nesse sentido, a “Vitrine Tecnológica” é uma forma de mostrar o protagonismo da
                                instituição como agente transformador da realidade local, nacional e
                                internacional.

                                A plataforma está estruturada de maneira a apresentar os Pesquisadores, as
                                Propriedades Intelectuais e os Laboratórios divididos por área do conhecimento
                                em que este se insere. ​</p>
                        </div>
                        <hr/>
                    </div>
                    <div class="col col-md-1"></div>
                </div>
        </main>
    </div>
</div>
<!--
<div class="row">
    <div class="col col-md-2"></div>
    <div class="col col-md-8">
        <div class="row">
            <div class="col col-md-4">
                <div class="pesquisa"></div>
                <h1 class="titulopesquisa">Laboratórios</h1>
                <div style="text-align:center;">
                    <i class="fas fa-flask icones"></i>
                    <br/>
                    <a href="laboratorio/index" class="pesquisarlab btn btn-lg btn-outline-success">Pesquisar
                        laboratórios</a>
                </div>
            </div>
            <div class="col col-md-2"></div>
            <div class="col col-md-6">
                <div class="pesquisadescricao"></div>
                <a> Local provido de instalações, aparelhagem e produtos
                    necessários a manipulações, exames e experiências
                    efetuados no contexto de pesquisas científicas, de
                    análises médicas, análises de materiais ou de ensino
                    científico e técnico.atividade que envolve observação, experimentação ou
                    produção num campo de estudo (p.ex., o comportamento animal) ou a
                    prática de determinada arte ou habilidade ou estudo; oficina.
                    "l. de música***</a>
            </div>
            <div class="col col-md-1"></div>
        </div>
        <hr/>
        <div class="row">
            <div class="col col-md-6">
                <div class="pesquisadescricao"></div>
                <a> Aquele que ensina uma arte, uma atividade, uma ciência, uma língua, etc.
                    ,Pessoa que ensina em escola, universidade ou noutro estabelecimento de
                    ensino.,Executante de uma orquestra de primeira ordem.,Aquele que professa
                    publicamente as verdades religiosas.,Entendido, perito.,Que ensina.,professor
                    livre: o que ensina sem estipêndio do governo.</a>
            </div>

            <div class="col col-md-2"></div>
            <div class="col col-md-4">
                <div class="pesquisa"></div>
                <h1 class="titulopesquisa">Professores</h1>
                <div style="text-align:center;">
                    <i class="fas fa-book icones"></i>
                    <br/>
                    <a href="#" class="pesquisarlab btn btn-lg btn-outline-info">Pesquisar Professores</a>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col col-md-4">
                <div class="pesquisa"></div>
                <div style="text-align: center;">
                    <h1 class="titulopesquisa">Patentes</h1>
                </div>
                <div style="text-align:center;">
                    <i class="fas fa-folder-open icones"></i>
                    <br/>
                    <a href="#" class="pesquisarlab btn btn-lg btn-outline-danger">Pesquisar Patentes</a>
                </div>
            </div>
            <div class="col col-md-2"></div>
            <div class="col col-md-6">
                <div class="pesquisadescricao"></div>
                <a> A patente é o título conferido pelo Estado àquele
                    que inventou (produto ou processo) algo e que não
                    está contido nas proibições da lei. Saiba tudo sobre
                    patentes, como tipos, requisitos, vantagens, prazo e
                    tempo de duração de uma patente.</a>
            </div>
            <div class="col col-md-1"></div>
        </div>
        <div class="row">
            <div class="col col-md-4">
                <div class="pesquisa"></div>
                <div style="text-align: center;">
                    <h1 class="titulopesquisa">VUE</h1>
                </div>
                <div style="text-align:center;">
                    <i class="fas fa-folder-open icones"></i>
                    <br/>
                    <a href="#" class="pesquisarlab btn btn-lg btn-outline-danger">Botão</a>
                </div>
            </div>
            <div class="col col-md-2"></div>
            <div class="col col-md-6" id="minhapica">
                <div class="pesquisadescricao"></div>


                <!-- ====================== -->
<!--
                <div id="teste">
                    <label>Nome</label>
                    <input class="form-control" type="text" v-model="nome" placeholder="Digite o nome">
                    <br><br>

                    <label>Telefone</label>
                    <input class="form-control" type="text" v-model="telefone"
                           placehold="digite o telefone">
                    <br><br>

                    <input type="radio" name="radio1" id="1" value="valor1">
                    <label>Valor1</label>
                    <input type="radio" name="radio1" id="2" value="valor2">
                    <label>Valor2</label>
                    <input type="radio" name="radio1" id="3" value="valor3">
                    <label>Valor3</label>
                    <br><br>

                    <select class="custom-select" id="selecao">
                        <option selected>Choose...</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>

                </div>

                <!-- ====================== -->
<div class="post">
</div>
</main>
</div>
@include('Layouts.Footer')
</div>
</body>
</html>
