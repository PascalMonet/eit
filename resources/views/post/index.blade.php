<div class="row">
    <div class="col col-md-1 col-xs-1">
    </div>
    <div class="col col-md-2 col-xs-2">
        <div class="card post" style="width: 14rem; margin-left:2%;">
            <img class="card-img-top" src="images/noticias.png" alt="Card image cap">
        </div>
    </div>

    @foreach( $bd as $bds)
        <div style="margin-top:2%;" class="col col-md-2 col-xs-2">
            <div class="card post" style="width: 14rem; margin-left:2%;">
                <div class="zoom">
                    <a href="/eit/public/post/{{$bds->id}}">
                        <img class="card-img-top" src="images/3.jpg" alt="Card image cap">
                    </a>
                    <h5 style="text-align:center;margin-top:4%;" class="card-title titulonoticia">{{$bds->titulo}}</h5>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col col-md-1 col-xs-1"></div>

</div>
<div class="row">
    <div class="col col-md-8"></div>
    <div class="col col-md-4">
        <div class="maisinformacao">
            <a class="btn btn-lg btn-primary">
                Mais notícias
            </a>
        </div>
    </div>

</div>
<!--
<div style="text-align: center;margin-top:3%;">
    { !! $bd->render() !! }</div>
-->
