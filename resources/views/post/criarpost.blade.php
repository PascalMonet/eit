@extends('layouts.app')
@extends('layouts.link')

@section('content')
    <div class="container-fluid">
        <form method="post" class="form" action="{{route('post.store')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col col-md-2"></div>
                <div class="col col-md-8">
                    <div style="text-align: center;">
                        <h1>Criar post</h1>
                    </div>
                    <br/>
                    <hr/>
                    <br/>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <h3>Título</h3>
                        </div>
                        <div class="col col-md-9">
                            <input class="form-control" type="text" name="titulo" placeholder="Digite o título"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <h3>Autor</h3>
                        </div>
                        <div class="col col-md-9">
                            <input class="form-control" type="text" name="autor" placeholder="Digite seu Nome"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <h3>Descrição</h3>
                        </div>
                        <div class="col col-md-9">
                            <input class="form-control" type="text" name="descricao" placeholder="Digite a descricao"/>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <h3>Artigo</h3>
                        </div>
                        <div class="col col-md-9">
                            <textarea class="form-control-lg" name="post" style="resize: none;" cols="67" rows="10"
                                      placeholder="O artigo"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info btn-lg">Criar o Post</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col col-md-2"></div>
    </div>

    <script>
        var textareas = document.getElementsByTagName('textarea');
        var count = textareas.length;
        for (var i = 0; i < count; i++) {
            textareas[i].onkeydown = function (e) {
                if (e.keyCode == 9 || e.which == 9) {
                    e.preventDefault();
                    var s = this.selectionStart;
                    this.value = this.value.substring(0, this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
                    this.selectionEnd = s + 1;
                }
            }
        }
    </script>
    @endsection
    </div>