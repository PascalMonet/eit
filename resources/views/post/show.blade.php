<!DOCTYPE html>
@include('layouts.link')
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('Layouts.Nav')
</head>
<body>
<div class="container-fluid">
    <div class="tela">
        <main role="main" class="body">
            <h1 class="titulo">{{$post->titulo}}</h1>
            <div class="row">
                <div class="col col-md-2">
                </div>
                <div class="col col-md-8">
                    <card class="card-body">
                        {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($post->post) !!}
                    </card>
                </div>
                <div class="col col-md-2">
                </div>
            </div>
            <footer class="blockquote-footer"></footer>

        </main>
    </div>
</div>
</main>

@include('Layouts.Footer')
</div>
</body>
</html>
