<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>EIT</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <div class="navegacao">
        <nav class="navbar ">
            <ul class="navbar-nav espacamentoparaonav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li><a class="titulonav nav-link" href="{{ route('login') }}">{{ __('Entrar como Administrador') }}</a>
                </li>
                @else
                    <div class="row">
                        <div class="col col-md-6">
                            <a class="titulonav nav-link" href="{{route('home')}}"> Página Administrativa</a>
                        </div>
                        <div class="col col-md-6">
                            <li class="nav-item dropdown nomelogado">
                                <a id="navbarDropdown" class="nav-link titulonav dropdown-toggle" href="#"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                   v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('post.index') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Sair') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        </a>
                                        <button class="titulonav navbar-toggler" type="button" data-toggle="collapse"
                                                data-target="#navbarSupportedContent"
                                                aria-controls="navbarSupportedContent" aria-expanded="false"
                                                aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>
                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="navbar-nav ">
                                            </ul>
                                        @csrf
                                    </form>
                                </div>
                        </div>
                        </li>
                    </div>
                    @endguest
            </ul>
        </nav>

        <div class="titulonavegacao">
            <img src="images/EITazul .png" width="300px" height="100px" width: auto;
                 height: auto;>
        </div>
        <nav class="navbar navbar-expand-md ">
            <div class="menudenavegacao">
                <a class="titulonav navbar-brand" href="{{route('post.index')}}">
                    Inicio
                </a>
                <a class="titulonav navbar-brand">
                    /
                </a>
                <a class="titulonav  navbar-brand" href="{{route('laboratorio')}}">
                    Laboratórios
                </a>
                <a class="titulonav navbar-brand">
                    /
                </a>
                <a class="titulonav navbar-brand" href="#">
                    Contatos
                </a>
                <a class="titulonav navbar-brand">
                    /
                </a>
                <a class="titulonav navbar-brand" href="#">
                    Patentes
                </a>
                <a>
                    /
                </a>
                <a class="titulonav navbar-brand" href="{{route('quemsomos')}}">
                    Quem somos
                </a>
                    <!-- Right Side Of Navbar -->
            </div>
    </div>
    </nav>
</div>
</div>
</body>
</html>
