<!DOCTYPE html>
@include('layouts.link')
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('Layouts.Nav')
</head>
<body class="container-fluid">
<div class="">
    <main role="main" class="body">
        <div class="patentesemdestaque">
        </div>
        <div class="row">
            <div class="col col-md-12 col-xs-2">
                <h1 class=" titulodepatente">
                    Patentes em destaque
                </h1>
                <div class="slideprincipal">
                    @include('slideprincipal')
                </div>
            </div>
        </div>
        <div class="noticias">
            @include('post.index')
        </div>
        <!--@include('redesocial')-->
    @include('Layouts.Footer')
</div>
</main>
</div>
</body>
</html>


<!--
 Itens aleatórios

    Facebook



-->