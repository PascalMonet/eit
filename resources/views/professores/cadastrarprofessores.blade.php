<!DOCTYPE html>
@include('layouts.link')
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Escritório de inovação e tecnologia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('Layouts.Nav')
</head>
<body>

<!-- Menu, não mudar entre as novas abas -->


<!-- Conteudo do menu    inner cover   inicial-->

<div class="container-fluid">
    <div class="tela">
        <main role="main" class="body">
            <h1 class="titulo">Cadastro de Professores</h1>
            <div class="row">
                <div class="col col-md-3"></div>
                <div class="col col-md-6">
                    <form method="post" class="form" action="{{route('laboratorio.store')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="row ">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Nome do Professor(a)</label>
                                </div>
                                <div class="col col-md-9 ">
                                    <input type="text" class="form-control" name="nome" placeholder="Digite o nome">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Área de atuação: </label>

                                </div>
                                <div class="col col-md-9">
                                    <input required type="text" class="form-control" name="area_atuacao"
                                           placeholder="Digite a área">
                                </div>
                            </div>
                            <!-- Descrição -->
                            <div class="row" style="margin-bottom:10%;">
                                <div class="col col-md-3">
                                    <label>sexo: </label>
                                </div>
                                <div class="col col-md-9">
                                    <input type="radio" name="sexo" value="Homem" checked> Homem<br>
                                    <input type="radio" name="sexo" value="Mulher"> Mulher<br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>E-mail: </label>
                                </div>
                                <div class="col col-md-9">
                                    <input class="form-control" name="telefone" required
                                           placeholder="Digite o E-mail">
                                </div>
                            </div>
                            <!-- E-mail -->
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Link do cuurriculus Lattes </label>
                                </div>
                                <div class="col col-md-9">
                                    <input class="form-control" type="email" name="email" required
                                           placeholder="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=xxxx">
                                </div>
                            </div>
                            <!-- Atendimento -->
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Campus </label>
                                </div>
                                <div class="col col-md-9">
                                    <input type="radio" name="Campus" value="Cuiaba" checked> Cuíaba<br>
                                    <input type="radio" name="Campus" value="Barra do Garca"> Barra do Garças<br>
                                    <input type="radio" name="Campus" value="sinop"> Sinop<br>
                                    <hr/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Lotação </label>
                                </div>
                                <div class="col col-md-9">
                                    <input type="text" class="form-control" name="lotacao" requerid placeholder="ex: Instituto de Química/Biologia/Física">

                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Formação </label>
                                </div>
                                <div class="col col-md-9">
                                    <div class="form-group">
                                        <select class="form-control" id="Formacao">
                                            <option>Doutorado</option>
                                            <option>Mestrado</option>
                                            <option>Especialização Nivel Superior</option>
                                        </select>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-md-3 CadastroLab">
                                    <label>Jornada </label>
                                </div>
                                <div class="col col-md-9">
                                    <div class="form-group">
                                        <select class="form-control" id="Jornada">
                                            <option>20</option>
                                            <option>40</option>
                                            <option>DE</option>
                                        </select>
                                        <br/>
                                        <label>DE* Dedicação Exclusiva</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="posicaobotao">
                            <button class="btn btn-danger btn-lg">Cancelar</button>
                            <button type="submit" class="btn btn-info botaoseguinte btn-lg">Cadastrar</button>
                        </div>
                        <!-- Projetos Ver como adicionar vários projetos.-->
                    </form>
                </div>
                <div class="col col-md-3"></div>
            </div>
        </main>
    </div>
    @include('Layouts.Footer')
</div>
</body>
</html>
