@extends('layouts.app')
@extends('layouts.link')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="alert alert-success">
                    @if (session('status'))
                    @endif
                    Seja bem-vindo, {{ Auth::user()->name }}!
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col col-md-3"></div>
            <div class="col col-md-6">

                <h1 class="text-center titulocadastrar"><i class="fas fa-database"></i>

                    Cadastrar</h1>
                <div class="coluna">
                    <a class="btn btn-lg btn-outline-primary" href="{{route('laboratorios.create')}}">
                        Cadastrar novos laboratórios
                    </a>
                    <a class="cadastrarprofessores btn btn-lg btn-outline-primary" href="#">
                        Cadastrar novos Professores
                    </a>
                    <a class="cadastrarprofessores btn btn-lg btn-outline-primary" href="#">
                        Cadastrar novos membros
                    </a>
                </div>
                <h1 class="text-center titulocadastrar"><i class="fas fa-newspaper"></i>Post</h1>
                <div class="coluna">
                    <a class="btn btn-lg btn-outline-warning" href="{{route('post.create')}}">
                        Criar novo Post
                    </a>
                </div>
                <hr/>
                <!-- Editar -->
                <h1 class="text-center titulocadastrar"><i class="fas fa-cogs"></i>
                    Listar/Editar/Remover</h1>

                <div class="coluna">
                    <a class="cadastrarprofessores btn btn-lg btn-outline-success" data-toggle="collapse"
                       href="#listarlaboratorio" role="button"
                       aria-expanded="false" aria-controls="collapseExample">
                        Listar Laboratórios
                    </a>
                    <a class="cadastrarprofessores btn btn-lg btn-outline-success" data-toggle="collapse"
                       href="#listarprofessores" role="button"
                       aria-expanded="false" aria-controls="collapseExample">
                        Listar Professores
                    </a>
                    <a class="cadastrarprofessores btn btn-lg btn-outline-success" data-toggle="collapse"
                       href="#listarmembros"
                       role="button"
                       aria-expanded="false" aria-controls="collapseExample">
                        Listar Membros
                    </a>
                </div>
                <div class="collapse" id="listarlaboratorio">
                    <div class="card card-body">
                        <table class="table ">
                            <tr>
                                <th scope="col">
                                    Título
                                </th>
                                <th scope="col">
                                    Área de atuação
                                </th>
                                <th scope="col">
                                    Local
                                </th>
                                <th scope="col">
                                    Funções
                                </th>
                            </tr>
                            <tbody>
                            <tr>
                                @foreach($bd as $bds)
                                    <td scope="row">
                                        {{$bds->titulo}}
                                    </td>
                                    <td scope="row">
                                        {{$bds->area_atuacao}}
                                    </td>
                                    <td scope="row">
                                        {{$bds->local}}
                                    </td>
                                    <td scope="row">
                                        <a style="color:#000; margin-right:30px" class="fas fa-wrench"
                                           href="/eit/public/editarlaboratorios/{{$bds->id}}"></a>
                                        <a class="fas fa-trash-alt"></a>

                                    </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="collapse" id="listarprofessores">
                    <div class="card card-body">
                        Professores Listados
                    </div>
                </div>
                <div class="collapse" id="listarmembros">
                    <div class="card card-body">
                        Membros Listados
                    </div>
                </div>
            </div>

            <hr/>

            <!-- Remover -->
            <div class="col col-md-3"></div>

        </div>
        @endsection
    </div>