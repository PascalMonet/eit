<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','PostController@index');
Route::resource('/post','PostController' );

Route::get('/patentes',function(){
    return view('patentes.index');
});
Route::get('/laboratorio','LaboratorioController@index')->name('laboratorio');
Route::resource('laboratorios','LaboratorioController');
Auth::routes();
Route::get('/quemsomos',function(){
    return view('quemsomos');
})->name('quemsomos');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth']], function (){
    Route::get('/create', 'PostController@create');
    Route::get('/cadastrarprofessores',function(){
        return view('professores.cadastrarprofessores');
    });
    Route::get('/Home','HomeController@index')->name('bd');

});
